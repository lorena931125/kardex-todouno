# MS Core Engine

Cyberbank Core Engine microservice.

# Core Engine
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-core/Core%20Engine/Core%20Engine%20%20Build-starterkit?repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=2392&repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)
# Table of Contents
- [Project Overview](#markdown-header-project-overview)
- [Technology stack](#markdown-header-technology-stack)
- [Configuration](#markdown-header-configuration)
    - [Language and error translations](#markdown-header-language-and-error-translations)
    - [Databases](#markdown-header-databases)
        - [Business](#markdown-header-business)
        - [Services](#markdown-header-services)
  - [Security](#markdown-header-security)
    - [Socket](#markdown-header-socket)
    - [HTTPS](#markdown-header-https) 
  - [Filesystem settings](#markdown-header-filesystem-settings)
  - [Authentication settings](#markdown-header-authentication-settings)
- [Kafka Installation](#markdown-header-kafka-installation)
# Project Overview
Cyberbank Core Engine is the main component of Cyberbank Core, acting as the interpreter of the business rules mapped through service compositions specified by the user. The Engine is a Java standalone application that reads and executes the metadata generated using Cyberbank CPG tool. 
# Technology stack
* Maven
* Java 11
* Supported Databases: SQL Server & Oracle


# Configuration
The following properties must be configurated into values.yaml
### Language and error translations
Property|Value
---|---
engine.errors.language| Language chosen to show errors
engine.errors.locales| Locales for error translations
engine.runtime.language| Language chosen to general messages
engine.datamodel.translate.enabled| Enables or disables the transtation of de Data model
engine.datamodel.translate.language| Id number of language chosen to show Data model. Currently 1=Spanish, 2=English.

### Databases
Property|Value
---|---
hibernate.hikari.connectionTimeout| This property controls the maximum number of milliseconds that a client (that's you) will wait for a connection from the pool. If this time is exceeded without a connection becoming available, a SQLException will be thrown. Lowest acceptable connection timeout is 250 ms. Default: 30000 (30 seconds)
hibernate.hikari.autoCommit| This property controls the default auto-commit behavior of connections returned from the pool. It is a boolean value. Default: true
hibernate.hikari.minimumIdle| This property controls the minimum number of idle connections that HikariCP tries to maintain in the pool. If the idle connections dip below this value and total connections in the pool are less than maximumPoolSize, HikariCP will make a best effort to add additional connections quickly and efficiently. However, for maximum performance and responsiveness to spike demands, we recommend not setting this value and instead allowing HikariCP to act as a fixed size connection pool. Default: same as maximumPoolSize
hibernate.hikari.maximumPoolSize| This property controls the maximum size that the pool is allowed to reach, including both idle and in-use connections. Basically this value will determine the maximum number of actual connections to the database backend. A reasonable value for this is best determined by your execution environment. When the pool reaches this size, and no idle connections are available, calls to getConnection() will block for up to connectionTimeout milliseconds before timing out. Please read about pool sizing. Default: 10
hibernate.hikari.idleTimeout| This property controls the maximum amount of time that a connection is allowed to sit idle in the pool. This setting only applies when minimumIdle is defined to be less than maximumPoolSize. Idle connections will not be retired once the pool reaches minimumIdle connections. Whether a connection is retired as idle or not is subject to a maximum variation of +30 seconds, and average variation of +15 seconds. A connection will never be retired as idle before this timeout. A value of 0 means that idle connections are never removed from the pool. The minimum allowed value is 10000ms (10 seconds). Default: 600000 (10 minutes)
hibernate.hikari.connectionTimeout| This property controls the maximum number of milliseconds that a client (that's you) will wait for a connection from the pool. If this time is exceeded without a connection becoming available, a SQLException will be thrown. Lowest acceptable connection timeout is 250 ms. Default: 30000 (30 seconds)
hibernate.connection.provider_class| org.hibernate.hikaricp.internal.HikariCPConnectionProvider
hibernate.id.new_generator_mappings| false

#### Business
Property|Value
---|---
engine.db.connection_string|  Connection string to database.  If is absent, this is taken from cfg.xml
engine.db.vendor| Database engine provider name. Possible values| ORACLE, SQLSERVER(Case insensitive)
engine.db.hibernate.dialect|  Database dialect. e.g. org.hibernate.dialect.OracleDialect, org.hibernate.dialect.SQLServerDialect, org.hibernate.dialect.DB2Dialect
engine.db.schema|  Database schema.  If absent, user name is used.
engine.db.user|  Database username.
engine.db.password|  Database password.
engine.db.max_results|  Limit of result by query. If is absent or 0, the result hasn�t limit.
engine.db.driver_class|  JDBC Driver for database interaction. e.g. oracle.jdbc.OracleDriver, org.postgresql.Driver, COM.ibm.db2.jdbc.app.DB2Driver
engine.db.use_interceptor| Enable or disable logging interceptor
engine.db.interceptor_class| Logging interceptor Java class
engine.db.interceptor.auditloginterceptor.manager| Audit logging interceptor manager Java class

#### Services
Property|Value
---|---
cpg.db.hibernate_cfg_xml|  Filename for Hibernate configurations and mapping to services database.
cpg.db.connection_string| Connection string to services database.
cpg.db.vendor| Database engine provider name.
cpg.db.user| Database username.
cpg.db.password|  Database password.
cpg.db.driver_class|  JDBC Driver for services database interaction. e.g. oracle.jdbc.OracleDriver, org.postgresql.Driver, COM.ibm.db2.jdbc.app.DB2Driver
cpg.db.hibernate.dialect|  Services database dialect. e.g. org.hibernate.dialect.OracleDialect, org.hibernate.dialect.SQLServerDialect, org.hibernate.dialect.DB2Dialect, org.hibernate.dialect.PostgreSQLDialect
cpg.db.schema|  Database schema.  If is absent, user name is used.

### HTTP Server settings
Property|Value
---|---
engine.http.server.enabled| Enable or disable http access to the Engine.
engine.http.server.port| Port number to listen.
engine.http.server.host| Host name
engine.http.server.idletimeout| Waiting time for response in millisecond 
engine.http.server.threads.max| Maximum pool size of threads
engine.http.server.threads.min=Minimum pool size of threads

### Security

#### Socket
Property|Value
---|---
engine.transaction.socket.ssl.enabled| Enables or disables secured communication
engine.transaction.socket.ssl.keystore.file| Keystore Java path
engine.transaction.socket.ssl.keystore.password| Keystore password
engine.transaction.socket.ssl.truststore.file| Truststore (Warehouse of trust certificates) path
engine.transaction.socket.ssl.truststore.password| Truststore password

#### HTTPS
Property|Value
---|---
engine.https.server.enabled| Enables or disables CORE-CHANNEL communication by HTTPS
engine.https.server.host| Host name
engine.https.server.port| Secure port of core transactions. By default it is 8443
engine.https.server.keystore.path| Certificate Keystore Java
engine.https.server.keystore.password| Keystore certificate password
engine.https.server.keymanager.password| Certificate key store
engine.https.server.truststore.path| Warehouse of recognized certificates
engine.https.server.truststore.password| Certified certificates key

### Filesystem settings
Property|Value
---|---
engine.fs.path.services| Service folder path
engine.fs.path.errors| Error message folder path
engine.fs.path.language| Language folder path

### Authentication settings
Property|Value
---|---
engine.auth.request.time| Timeout for authentication requests in seconds. Default 2.
engine.auth.enabled| Enable or disable authentication by HMac. Default false.

### Kafka 
Property|Value
---|---
engine.monitor.kafka.topic| Topic to which the monitor will subscribe
engine.monitor.kafka.bootstrap.servers | Kafka address in  format IP: PORT where it is running in the cluster or VM
engine.monitor.kafka.client.id | Identifier of the client to which it connects.

### General configuration
Property|Value
---|---
engine.interface.project.alias| Load operation only for the project alias set. Default Tech, set NONE for disable
engine.monitor.use.logstash| true for enabled monitor commnunitation via ELK 
region.project.alias| Load operation only for the region alias set. Set NONE for disable
engine.operations.loader| Java class for Operations loader factory. For DB connection  net.technisys.cyberbank.core.engine.service.OperationsLoaderFactory. For filesystem           net.technisys.cyberbank.core.engine.service.OperationsJsonFSLoader.
auditable.entity.status| Entity status required to be auditable.
engine.folder.entities | Internal folder where core read entities. Default /home/technisys/entities
engine.folder.basicservices | Internal folder where core read basicservices. Default /home/technisys/basicservices
engine.folder.extensions | Internal folder where core read extensions. Default /home/technisys/extensions
engine.monitor.handler | configure which technology  will be used for the core-monitor. ( Default:NONE) it can have the next options (KAFKA,NONE,ELASTIC,LOGBACK)

# Kafka installation

from platform version 3.5.1 or higher, you can configure the core-engine with Kafka for the core-monitor logs.

For install it you must execute the following commands:
## Add repo confluentinc
helm repo add confluentinc https://confluentinc.github.io/cp-helm-charts/

## Update repo
helm repo update

## Install kafka
helm install tec-confluent-oss confluentinc/cp-helm-charts --set cp-kafka-rest.enabled=false,cp-kafka-connect.enabled=false,cp-ksql-server.enabled=false,cp-control-center.enabled=false,cp-schema-registry.enabled=false,cp-control-center.prometheus.jmx.enabled=false,cp-zookeeper.persistence.enabled=false,cp-zookeeper.prometheus.jmx.enabled=false,cp-kafka.persistence.enabled=false,cp-kafka.prometheus.jmx.enabled=false,cp-schema-registry.prometheus.jmx.enabled=false,cp-kafka.nodeport.enabled=true -n <NAMESPACE>
```

By default 3 instances of zookeeper and kafka are deployed. For expose each kafka broker service, patch k8s service with:
```
## Patch broker 0
kubectl patch svc tec-confluent-oss-cp-kafka-0-nodeport -p '{"metadata": { "annotations": { "service.beta.kubernetes.io/azure-load-balancer-internal": "true", "service.beta.kubernetes.io/azure-load-balancer-internal-subnet": "<SUBNET_NAME_ILB>"  } }, "spec": { "type": "LoadBalancer" } }' -n <NAMESPACE>

## Patch broker 1
kubectl patch svc tec-confluent-oss-cp-kafka-1-nodeport -p '{"metadata": { "annotations": { "service.beta.kubernetes.io/azure-load-balancer-internal": "true", "service.beta.kubernetes.io/azure-load-balancer-internal-subnet": "<SUBNET_NAME_ILB>"  } }, "spec": { "type": "LoadBalancer" } }' -n <NAMESPACE>

## Patch broker 2
kubectl patch svc tec-confluent-oss-cp-kafka-2-nodeport -p '{"metadata": { "annotations": { "service.beta.kubernetes.io/azure-load-balancer-internal": "true", "service.beta.kubernetes.io/azure-load-balancer-internal-subnet": "<SUBNET_NAME_ILB>"  } }, "spec": { "type": "LoadBalancer" } }' -n <NAMESPACE>
```
## Additional configuration

when the kafka it's installed, configure the next feature on kafka installation :

Property|Value
---|---
KAFKA_AUTO_LEADER_REBALANCE_ENABLE | 'true'

this configuration allows each Kafka instances recognize the leader

Property|Value
---|---
NAMESPACE | Name of the namespace in the cluster where the Kafka instance runs
SUBNET_NAME_ILB | Name of the subnet for internal balancer.

## Code analysis
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=technisys_runtime-core-engine&metric=alert_status&token=0c360021df60d3db13d01706ec062a3c7de4f602)](https://sonarcloud.io/dashboard?id=technisys_runtime-core-engine)

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=technisys_runtime-core-engine&token=0c360021df60d3db13d01706ec062a3c7de4f602)](https://sonarcloud.io/dashboard?id=technisys_runtime-core-engine)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=technisys_runtime-core-engine&metric=bugs&token=0c360021df60d3db13d01706ec062a3c7de4f602)](https://sonarcloud.io/dashboard?id=technisys_runtime-core-engine)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=technisys_runtime-core-engine&metric=vulnerabilities&token=0c360021df60d3db13d01706ec062a3c7de4f602)](https://sonarcloud.io/dashboard?id=technisys_runtime-core-engine)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=technisys_runtime-core-engine&metric=code_smells&token=0c360021df60d3db13d01706ec062a3c7de4f602)](https://sonarcloud.io/dashboard?id=technisys_runtime-core-engine)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=technisys_runtime-core-engine&metric=coverage&token=0c360021df60d3db13d01706ec062a3c7de4f602)](https://sonarcloud.io/dashboard?id=technisys_runtime-core-engine)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=technisys_runtime-core-engine&metric=duplicated_lines_density&token=0c360021df60d3db13d01706ec062a3c7de4f602)](https://sonarcloud.io/dashboard?id=technisys_runtime-core-engine)

## Pipelines Status

 CICD     | DEV         | DEV-SP         | TEST             | DEMO 
----------|-------------|----------------|------------------|---------------
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-core/Core-Engine?repoName=technisys%2Fruntime-core-engine&branchName=develop)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=728&repoName=technisys%2Fruntime-core-engine&branchName=develop) | ![Deploy DEV](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/103/272) | ![Deploy DEV-SP](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/103/273)

