-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-12-2019 a las 02:56:42
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

CREATE DATABASE kardex;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `kardex`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` varchar(120) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `creation_date` date NOT NULL,
  `creation_user` varchar(100) NOT NULL,
  `bar_code` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `creation_date`, `creation_user`, `bar_code`) VALUES
('094e867c-82ff-4edf-be93-7a92c64cbf33', 'prueba', 'asd', '2019-12-08', 'test', 'a'),
('123', 'test', 'ss', '2019-12-09', 'ss', '111'),
('475ed463-b59d-4d5b-81bd-a3d3430efd78', 'prueba', 'asd', '2019-12-08', 'test', 'a');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales`
--

CREATE TABLE `sales` (
  `id` varchar(120) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sales`
--

INSERT INTO `sales` (`id`, `quantity`, `total`) VALUES
('057a37fb-0e57-4fb7-b8b5-3ea2e84c9d9c', 1, '1'),
('0a0317c4-be7d-450d-a66c-c4e430d3a47f', 50, '1000'),
('0efaeb9d-49ec-47e6-8a5d-31a0e3594093', 1, '1'),
('15febce7-8adf-4d87-b4d4-514aadd87da1', 50, '1000'),
('1757daee-a842-4829-ac1a-0b6abf5b26d5', 1, '1000'),
('33cdd594-ec35-410a-8bcf-526d7bfb2b0f', 4, '1000'),
('3f24f387-86fc-4f69-9298-e62f75225c3b', 10, '10000'),
('95f7145e-c9b6-4c45-b797-f4216c11fe06', 1, '1'),
('dce4be95-aa17-4b3c-9bab-56562e421f5b', 1, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales_stock`
--

CREATE TABLE `sales_stock` (
  `sales_id` varchar(120) NOT NULL,
  `stock_id` varchar(120) NOT NULL,
  `id` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sales_stock`
--

INSERT INTO `sales_stock` (`sales_id`, `stock_id`, `id`) VALUES
('3f24f387-86fc-4f69-9298-e62f75225c3b', 'e327dc46-77a1-4933-a1a7-4fbe84e46236', '21fc4a05-8f51-4be5-97de-584e0a977b28'),
('33cdd594-ec35-410a-8bcf-526d7bfb2b0f', '731f4fa0-5215-43ec-864e-28157f69f227', '654a8d4c-11d7-42d7-93ac-93cb7b03120a'),
('15febce7-8adf-4d87-b4d4-514aadd87da1', '28451524-9f6f-4919-96a4-d60e92378611', 'a96a3114-edc9-4a7d-8562-678d7682bb26'),
('1757daee-a842-4829-ac1a-0b6abf5b26d5', '28451524-9f6f-4919-96a4-d60e92378611', 'e76e1489-44ed-4ebd-9e40-4f756a5d6493');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock`
--

CREATE TABLE `stock` (
  `id` varchar(120) NOT NULL,
  `quantity` int(11) NOT NULL,
  `value` decimal(10,0) NOT NULL,
  `total` decimal(11,0) NOT NULL,
  `product_id` varchar(120) NOT NULL,
  `creation_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `stock`
--

INSERT INTO `stock` (`id`, `quantity`, `value`, `total`, `product_id`, `creation_date`) VALUES
('28451524-9f6f-4919-96a4-d60e92378611', 0, '1000', '10000', '094e867c-82ff-4edf-be93-7a92c64cbf33', '2019-12-08'),
('5568d5a0-9c94-4c3f-93d7-ffe8a8945680', 0, '13', '13', '094e867c-82ff-4edf-be93-7a92c64cbf33', '2019-12-08'),
('731f4fa0-5215-43ec-864e-28157f69f227', 96, '1000', '100000', '094e867c-82ff-4edf-be93-7a92c64cbf33', '2019-12-08'),
('a6bca3ee-322d-4a03-b418-667d0074959e', 0, '2', '2', '094e867c-82ff-4edf-be93-7a92c64cbf33', '2019-12-08'),
('d2928b78-9991-4b6a-84ca-027496020313', 10, '1', '10', '123', '2019-12-08'),
('e327dc46-77a1-4933-a1a7-4fbe84e46236', 0, '200', '1000', '123', '2019-12-08');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sales_stock`
--
ALTER TABLE `sales_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productId` (`product_id`),
  ADD KEY `productId_2` (`product_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
