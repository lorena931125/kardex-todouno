import { Component, OnInit ,Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ServiceConnectorService} from '../service-connector.service';
import { SaleProduct } from '../model/sale-product';
export interface DialogData {
  id: string;
  name: string;
}
@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})
export class SaleComponent implements OnInit {
  quantityAvailable:any;
  quantity:any;
  total:any;
  error:string;
  saleProduct:SaleProduct;
  status:boolean;
  constructor(public dialogRef: MatDialogRef<SaleComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData, public api:ServiceConnectorService) { }

  ngOnInit() {
    this.saleProduct=new SaleProduct();
    this.api.getSumStock(this.data.id).subscribe( (data:any) => {
      console.log(data)
     this.quantityAvailable=data;
    });
  }

  save(){
    console.log(this.quantityAvailable);
    debugger;
    if(this.quantity >this.quantityAvailable ){
      this.status=true;
      this.error="No es posible realiza la venta la cantidad ingresada es mayor al stock del producto";
    }else{
      this.error="";
      this.saleProduct.productId=this.data.id;
      this.saleProduct.quantity=this.quantity;
      this.saleProduct.total=this.total;
      this.api.saveSale(this.saleProduct).subscribe((data:any)=>{
        console.log(data);
        if(data != null){
          this.status=false;
          this.error="Se ha ingresado la venta";
          let diag = this.dialogRef;
          setTimeout(function(){
            diag.close()
          },500);
          
        }
      })

    }
  }

}
