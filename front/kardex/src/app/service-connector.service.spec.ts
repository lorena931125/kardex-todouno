import { TestBed } from '@angular/core/testing';

import { ServiceConnectorService } from './service-connector.service';

describe('ServiceConnectorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceConnectorService = TestBed.get(ServiceConnectorService);
    expect(service).toBeTruthy();
  });
});
