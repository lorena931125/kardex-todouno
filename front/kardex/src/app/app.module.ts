import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { StockComponent } from './stock/stock.component';
import { SaleComponent } from './sale/sale.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AddproductComponent } from './products/addproduct/addproduct.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalComponent } from './modal/modal/modal.component';
import { 
  MatFormFieldModule, 
  MatDialogModule,
  MatInputModule,
  MatButtonModule,
 } from '@angular/material';
import { DialogOverviewExampleDialog } from './dialog-overview-example-dialog/dialog-overview-example-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    StockComponent,
    SaleComponent,
    AddproductComponent,
    ModalComponent,
    DialogOverviewExampleDialog,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
  ],
  entryComponents: [ DialogOverviewExampleDialog,AddproductComponent,SaleComponent ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
