import { Component, OnInit,Inject } from '@angular/core';
import {ServiceConnectorService} from '../service-connector.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DialogOverviewExampleDialog} from '../dialog-overview-example-dialog/dialog-overview-example-dialog.component'
import {AddproductComponent} from './addproduct/addproduct.component';
import {SaleComponent} from '../sale/sale.component';


export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products:Array<any>;

  constructor(private serviceConnectorService:ServiceConnectorService,public dialog: MatDialog) { }

  ngOnInit() {
    this.getAllProducts();

  }

  getAllProducts(){
    console.log('ssf')
    this.serviceConnectorService.getProducts().subscribe( (data:any) => {
      console.log(data)
     this.products=data;
    });
  }

  openAddProducts():void{
    
      const dialogRef = this.dialog.open(AddproductComponent, {
        width: '500px',
        maxHeight: '500px'
        
      });
  
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        
      });
  }


  openDialog(idProduct): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: {id: idProduct}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }

  openSales(idProduct): void {
    const dialogRef = this.dialog.open(SaleComponent, {
      width: '250px',
      data: {id: idProduct}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }

}



