import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ServiceConnectorService} from '../../service-connector.service';
import{ModalService} from '../../modal-service.service';
import{ProductDto} from '../../model/product-dto';
import { Product } from 'src/app/model/product';
import { Stocks } from 'src/app/model/stocks';
@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {
  productGroup: FormGroup;
  displayStock:boolean=false;
  productDto: ProductDto;
  product:Product;
  stocks:Array<Stocks>;
  stock:Stocks;
  element: HTMLElement;
  msg:string;
  constructor(private formBuilder: FormBuilder, private api: ServiceConnectorService,private modalService: ModalService) { }

  ngOnInit() {
    this.productDto=new ProductDto();
    this.product=new Product();
    this.stocks=[];
    this.stock=new Stocks();
    this.productGroup = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      barCode: ['', Validators.required],
      state: ['', Validators.required],
      quantity:[null],
      value:[null],
      total:[null],
     
    });

  }

  get f() { return this.productGroup.controls; }

  onSubmit() {
   // this.submitted = true;

    // stop here if form is invalid
    console.log(this.f)
    console.log(this.productGroup.invalid);
    if (this.productGroup.invalid) {
      return;
    }
    this.product.name=this.f.name.value;
    this.product.description=this.f.description.value;
    this.product.barCode=this.f.barCode.value;
    this.stock.quantity=this.f.quantity.value;
    
    this.stock.value=parseFloat(this.f.value.value);
    this.stock.total=parseFloat(this.f.total.value);
    this.stocks.push(this.stock);
   this.productDto.products=this.product;
    this.productDto.lstStocks=this.stocks;

    this.api.saveProduct(this.productDto).subscribe( (data:any) => {
      console.log(data)
      this.msg="Se ha ingresado el producto";
    
    });
  }

  openModal(id: string) {
    this.displayStock=true;
    
}

closeModal(id: string) {
    this.modalService.close(id);
}

calculateTotal(){
 var quan=parseInt(this.f.quantity.value);
 var t=parseFloat(this.f.value.value);
if(quan != undefined && t != undefined){
this.f.total.setValue(quan*t);

}
}

  

}
