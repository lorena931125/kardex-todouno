import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductsComponent} from './products/products.component';
import{StockComponent} from './stock/stock.component';
import{SaleComponent} from './sale/sale.component';
import {AddproductComponent} from './products/addproduct/addproduct.component';


const routes: Routes = [{
  path: '',
  component: ProductsComponent
},
{
  path: 'stock',
  component: StockComponent
},
{
  path: 'sale',
  component: SaleComponent
},
{
  path: 'addProduct',
  component: AddproductComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
