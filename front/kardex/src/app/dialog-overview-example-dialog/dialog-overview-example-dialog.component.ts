import { Component, OnInit ,Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Stocks } from '../model/stocks';
import{ServiceConnectorService} from '../service-connector.service';


export interface DialogData {
  id: string;
  name: string;
}
@Component({
  selector: 'app-dialog-overview-example-dialog',
  templateUrl: './dialog-overview-example-dialog.component.html',
  styleUrls: ['./dialog-overview-example-dialog.component.css']
})


export class DialogOverviewExampleDialog {
quantity:any;
value:any;
total:any;
stocks:Stocks;
msg:string;
msgStyle:string;
status:boolean;
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,public api:ServiceConnectorService) {
      console.log(data.id);
      this.stocks=new Stocks();
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  calculateTotal(){
    if(this.quantity != undefined && this.value != undefined){
      this.total=this.quantity*this.value;
    }
  }

  saveStock(){
    this.stocks.quantity=this.quantity;
    this.stocks.total=this.total;
    this.stocks.value=this.value;
    if(this.quantity != undefined &&  this.total!= undefined && this.value!= undefined ){
      this.msg="";
      this.stocks.productId=this.data.id;
      var StockDto={"stock":this.stocks};
      this.api.saveStock(StockDto).subscribe( (data:any) => {
        if(data != null){
          this.status=false;
          this.msg="Se ingreso el stock";
          let diag = this.dialogRef;
          setTimeout(function(){
            diag.close()
          },500);
        }
      
      });
    }else{
      this.status=true;
      this.msg="Es obligatorio el ingreso de cantidad, total y valor unitario";
    }
   
    

  }   




}
