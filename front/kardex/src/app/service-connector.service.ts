import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { ProductDto } from './model/product-dto';
import { SaleProduct } from './model/sale-product';

@Injectable({
  providedIn: 'root'
})
export class ServiceConnectorService {
  public url:string;

  constructor(private http : HttpClient){
    this.url = environment.urlServices;
  }

  getProducts(){
    console.log(this.url);
    return this.http.get(this.url+"product/list");
  }

  saveProduct(productdto:ProductDto){
    return this.http.post(this.url+"product/save",productdto)
  }
  saveStock(stock:any){
    return this.http.post(this.url+"stock/add",stock)
  }
  saveSale(sale:SaleProduct){
    return this.http.post(this.url+"product/sale",sale)
  }

  getSumStock(productId){
    console.log(this.url);
    return this.http.get(this.url+"stock/sum?productId="+productId);
  }


 
}
